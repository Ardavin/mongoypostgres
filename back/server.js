var express = require('express');
var cors =require('cors');

  app = express();
  app.use(cors());

  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=kE1Ef_8i9fSFXF-hpDHQsK9GLT6HYKxZ";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);

//BLOQUE MONGODB...
var mongoClient =require('mongodb').MongoClient;
//var url = "mongodb://localhost:27017/local"; //si es local, localhost, pero si es en contenedor el nombre de la maquina dnde esta cn la misma red

var url = "mongodb://servermongo:27017/local"

//postgre
var pg =require('pg');
//ejecutandolo desde fuera del contenedor.

var urlusuarios = "postgres://docker:docker@localhost:5433/bdseguridad" //ojo que dependen si lo ejecutamos desde el cntentedo o no.
//esto mejor hacerlo en el post
//ejecutandolo desde dentro del terminal.


//req.body ={usuario:xxx,password:xxx}

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/movimientosmongo', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));

    mongoClient.connect(url,function(err,db){
        if (err){
          console.log(err);
        }else{
            var col=db.collection('movimientos');
            col.find({}).limit(3).toArray(function(err,docs){
              res.send(docs);
            });
            db.close();
        }


    });





    // clienteMlab.get('', function(err, resM, body) {
    //   if(err)
    //   {
    //     console.log(body);
    //   }
    //   else
    //   {
    //     res.send(body);
    //   }
    // });
});


app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('&f={"idcliente":1, "nombre": 1, "apellidos": 1}', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});


app.post('/login',function(req,res){
   //crear cliente postgre
   var clientePostgre = new pg.Client(urlusuarios);
   clientePostgre.connect();
   var resultado=0;

console.log('aqui');
   //hacer consulta
  const query =clientePostgre.query('SELECT COUNT (*) FROM usuarios WHERE login=$1 AND password=$2;',[req.body.login,req.body.password], function (err,result){
console.log('aqui 2');
 if (err){
  console.log(err);
  res.send(err);

 }else{
   //resul.rows[0] son los ersultados, aunque sea un count, siempre lo envian en filas.
  console.log(result.rows[0]);

  //hay que capturar los posibles errores.
  res.send(result.rows[0]);

 }


  })
   //devovler resultado




})

app.post('/movimientosmongo', function(req, res) {
  /* Crear movimiento en Mongo */
  mongoClient.connect(url, function(err,db){
    var col=db.collection('movimientos');
    console.log('intentando insertar en servidor');
    db.collection('movimientos').insertOne({a:1},function (err,r){
        console.log(r.insertedCount + 'registros insertados');
    });
    db.collection('movimientos').insertMany([{a:2},{a:3}],function(err,r){
      console.log(r.insertedCount+ 'registros insertados');
    });
    db.collection('movimientos').insertOne(req.body,function(err,r){
        console.log(r.insertedCount+ 'registros insertados body');
    });
    db.close();
    res.send("ok");
  });

});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
})
